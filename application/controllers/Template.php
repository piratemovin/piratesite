<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {
	public function index() {
    //Uncomment the below line to set title of the webpage. Default value is "Indian Pirates"
		//$data['title'] = "";

		//Uncomment the below line to set description of the webpage. Default value is "The Official Website of Indian Pirates".
		//$data['description'] = "";

    //Uncomment the below line to set author of the webpage. Default value is "Indian Pirates Web Team"
		//$data['author'] = "";

    //The content here corresponds to the view /application/templates/page-template.php. You will need to replace it to point to your view.
		$data['content'] = "templates/page";

    //You don't need to change this unless you really know what you are doing.
		$this->load->view('templates/default',$data);
	}
}

//NOTE: You don't need to use the PHP closing tags at the end of this file as it can result in undesired consequences.
//Read the official Codeigniter documentation to know more - http://www.codeigniter.com/user_guide/general/styleguide.html#php-closing-tag
