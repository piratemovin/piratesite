<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CI_Controller {
  public function chatrooms() {
    $data['title'] = "Chatrooms";
    $data['content'] = "social/chatrooms"; //This corresponds to /application/views/social/chatrooms.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
  }
}
