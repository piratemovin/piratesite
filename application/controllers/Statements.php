<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statements extends CI_Controller {

  public function index($page,$lang=null) {
    $data['title'] = 'Official Statements | Indian Pirates';
    if($lang) {
      $data['content'] = "statements/{$lang}/{$page}";
    } else {
      $data['content'] = "statements/{$page}";
    }
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
  }
}
