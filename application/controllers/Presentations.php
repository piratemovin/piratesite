<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentations extends CI_Controller {
	public function constitution() {
		$data['title'] = "Constitution";
		$data['description'] = "Basic principles, Organizational Structure and Processes";
		$data['author'] = "Pirate Praveen";
		$data['content'] = "templates/presentations"; //This corresponds to /application/views/templates/presentations.php
		$data['presentation_content'] = "presentations/constitution"; //This corresponds to /application/views/presentations/constitution.php
		$this->load->view('templates/presentations',$data); //This loads the view /application/views/templates/default.php
	}

	public function manifesto() {
		$data['title'] = "Manifesto";
		$data['description'] = "Indian Pirates Manifesto";
		$data['author'] = "Pirate Praveen";
		$data['content'] = "templates/presentations"; //This corresponds to /application/views/templates/presentations.php
		$data['presentation_content'] = "presentations/manifesto"; //This corresponds to /application/views/presentations/manifesto.php
		$this->load->view('templates/presentations',$data); //This loads the view /application/views/templates/default.php
	}
}
