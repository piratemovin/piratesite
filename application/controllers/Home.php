<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	/*
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://pirates.org.in/
	 */
	public function index() {
		$data['content'] = "home";
		$this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}

}
