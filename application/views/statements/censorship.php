<style type="text/css">
  @import url("<?php echo base_url(); ?>assets/css/page.css");
</style>
<section id="statement" class="page">
  <h2>On Indian Government Censorship of the Internet</h2>
  <h5 class="date">February 11, 2015</h5>
  <div class="content">
    <p>Recently many sites have been blocked by many Internet Service Providers <sup><a href="#link-1">[1]</a></sup>. We, Indian Pirates, consider this as a violation of our fundamental right to freedom of speech and expression guaranteed by the constitution.</p>
    <p>Censorship is characteristic of undemocratic nations like China, Iran and Soudi Arabia, should be viewed in contrast with how the governments of Sweden, Netherlands, Iceland, Switzerland, Brazil and many more countries provide their citizens completely unrestricted Internet <sup><a href="#link-2">[2]</a></sup>.</p>
    <p>Censorship is bad because when you lose freedom of speech you lose individual opinions! Without the free flow of ideas and thought, we miss beneficial ideas. If you look at history great ideas from people like Martin Luther King, that changed society were considered bad in their time. If people like him were censored we wouldn’t be having the freedoms we enjoy today.</p>
    <p>Online Censorship laws allow authorities to block large amounts of content in secrecy, without adequate demonstration of necessity, and in a disproportionate manner. The rules for blocking provide little by way of rights safeguards <sup><a href="#link-3">[3]</a></sup>. We don’t expect this government will listen to any of us, to uphold basic human rights. So we are left with only technical measures to exercise our fundamental rights.</p>
    <p>We recommend using censorship free DNS services like opennicproject.org and dns.watch to continue browsing the internet without interruption.</p>
    <p>We also anticipate more powerful censorship in the coming days, so we recommend everyone to start using tor (torproject.org). You may also use a vpn service like <a href="https://www.privatetunnel.com/" target="_blank">https://www.privatetunnel.com</a> or <a href="https://vpn.autistici.org/" target="_blank">https://vpn.autistici.org/</a> or <a href="https://help.riseup.net/en/vpn" target="_blank">https://help.riseup.net/en/vpn</a> for faster browsing without local censorship but they are not fully anonymous (vpn service providers can track you and they may be legally required to track you and provide your information when asked). You can also setup a VPN service yourself if you have access to a server outside India using Free Software like OpenVPN (http://openvpn.net).</p>
    <p>As a long term solution, we recommend getting involved in politics and stop expecting career politicians to stand up for our rights. Support groups and collectives that defend democracy and fight for human rights.</p>
    <p>If you really think blocking websites will stop terrorism, you are in a fool’s heaven. Almost all terrorists groups are sustained by state support. You can read <a href="https://en.wikipedia.org/wiki/State-sponsored_terrorism" target="_blank">https://en.wikipedia.org/wiki/State-sponsored_terrorism</a> for a list of terrorist groups supported by various countries.</p>
    <p>Indian Pirates is a collective fighting for human rights and civil liberties. You can join us at <a href="http://pirates.org.in">pirates.org.in</a>.</p>
  </div>
  <div id="references">
    <h3>References</h3>
    <ul class="citations">
      <li id="link-1">[1] <a href="http://techcrunch.com/2014/12/31/indian-government-censorsht/" target="_blank">http://techcrunch.com/2014/12/31/indian-government-censorsht</a></li>
      <li id="link-2">[2] <a href="https://en.wikipedia.org/wiki/Internet_censorship_and_surveillance_by_country" target="_blank">https://en.wikipedia.org/wiki/Internet_censorship_and_surveillance_by_country</a></li>
      <li id="link-3">[3] <a href="http://www.thehoot.org/web/Online-Censorship-laws-needs-reform/8035-1-1-7-true.html" target="_blank">http://www.thehoot.org/web/Online-Censorship-laws-needs-reform/8035-1-1-7-true.html</a></li>
    </ul>
  </div>
</section>
